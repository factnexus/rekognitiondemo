package demo;

/**
 * Configuration constants.
 *
 * @author nmenego 2017/03/02
 */
public interface Config {
    String PROFILE_NAME = "default";
    String BUCKET_NAME = "factnexus-test-rekognition";
    String REGION_NAME = "us-west-2";

    // images in my bucket
    String IMG_LENA = "lena.jpg";
}
