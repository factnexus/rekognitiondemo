package demo;


import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.List;

/**
 * Search for a face in a different photo.
 *
 * @author nmenego 2017/03/02
 */
public class CompareFacesUploadDemo {
    public static final String COLLECTION_ID = "collection-id";

    public static void main(String[] args) throws Exception {
        AWSCredentials credentials;
        try {
            credentials = new ProfileCredentialsProvider(Config.PROFILE_NAME).getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
                    + "Please make sure that your credentials file is at the correct "
                    + "location (/Users/<userid>/.aws/credentials), and is in valid format.", e);
        }

        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Config.REGION_NAME).build();

        //1. Add three faces to the collection.
        IndexFacesResult indexFacesResult = callIndexFaces(COLLECTION_ID, rekognitionClient, "ariane1.jpg");
        callIndexFaces(COLLECTION_ID, rekognitionClient, "lena.jpg");
//        callIndexFaces(COLLECTION_ID, rekognitionClient, "image3.jpg");

        Float threshold = 70F;
        int maxFaces = 2;

        //2. Retrieve face ID of the 1st face added.
        List<FaceRecord> faceRecordList = indexFacesResult.getFaceRecords();
        String faceId;
        if (faceRecordList != null && faceRecordList.size() > 0) {
            faceId = faceRecordList.get(0).getFace().getFaceId();
        } else {
            throw new IllegalArgumentException("No face found");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        //3. Search similar faces for a give face (identified by face ID).
        SearchFacesResult searchFacesResult = callSearchFaces(COLLECTION_ID, faceId, threshold, maxFaces, rekognitionClient);
        System.out.println(objectMapper.writeValueAsString(searchFacesResult));

        //4. Get an image object in S3 bucket.
        Image image = getImageUtil(Config.BUCKET_NAME, "ariane3.jpg");

        //5. Search collection for faces similar to the largest face in the image.
        SearchFacesByImageResult searchFacesByImageResult = callSearchFacesByImage(COLLECTION_ID, image, threshold, maxFaces, rekognitionClient);
        System.out.println(objectMapper.writeValueAsString(searchFacesByImageResult));
    }

    private static IndexFacesResult callIndexFaces(
            String collectionId, AmazonRekognition amazonRekognition, String name) {
        IndexFacesRequest req = new IndexFacesRequest()
                .withImage(getImageUtil(Config.BUCKET_NAME, name))
                .withCollectionId(collectionId)
                .withExternalImageId("externalId");

        return amazonRekognition.indexFaces(req);
    }

    private static SearchFacesResult callSearchFaces(String collectionId, String faceId, Float threshold, int maxFaces, AmazonRekognition amazonRekognition) {
        SearchFacesRequest searchFacesRequest = new SearchFacesRequest()
                .withCollectionId(collectionId)
                .withFaceId(faceId)
                .withFaceMatchThreshold(threshold)
                .withMaxFaces(maxFaces);
        return amazonRekognition.searchFaces(searchFacesRequest);
    }

    private static SearchFacesByImageResult callSearchFacesByImage(String collectionId, Image image, Float threshold, int maxFaces, AmazonRekognition amazonRekognition) {
        SearchFacesByImageRequest searchFacesByImageRequest = new SearchFacesByImageRequest()
                .withCollectionId(collectionId)
                .withImage(image)
                .withFaceMatchThreshold(threshold)
                .withMaxFaces(maxFaces);
        return amazonRekognition.searchFacesByImage(searchFacesByImageRequest);
    }

    private static Image getImageUtil(String bucket, String key) {
        return new Image()
                .withS3Object(new S3Object()
                        .withBucket(bucket)
                        .withName(key));
    }
}
